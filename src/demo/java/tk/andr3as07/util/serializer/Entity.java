package tk.andr3as07.util.serializer;

import tk.andr3as07.util.serializer.data.ObjectData;

public class Entity implements ISerializable {

	public float x, y, z;
	public String name;
	public long id;

	public Entity() {
	}

	public Entity(final float x, final float y, final float z, final String name, final long id) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.name = name;
		this.id = id;
	}

	@Override
	public void deserialize(final ObjectData object) {
		this.x = object.getPrimitiveField("x").getFloat();
		this.y = object.getPrimitiveField("y").getFloat();
		this.z = object.getPrimitiveField("z").getFloat();

		this.name = object.getPrimitiveField("name").getString();

		this.id = object.getPrimitiveField("id").getLong();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final Entity other = (Entity) obj;
		if (this.id != other.id)
			return false;
		if (this.name == null) {
			if (other.name != null)
				return false;
		} else if (!this.name.equals(other.name))
			return false;
		if (Float.floatToIntBits(this.x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(this.y) != Float.floatToIntBits(other.y))
			return false;
		if (Float.floatToIntBits(this.z) != Float.floatToIntBits(other.z))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (this.id ^ this.id >>> 32);
		result = prime * result + (this.name == null ? 0 : this.name.hashCode());
		result = prime * result + Float.floatToIntBits(this.x);
		result = prime * result + Float.floatToIntBits(this.y);
		result = prime * result + Float.floatToIntBits(this.z);
		return result;
	}

	@Override
	public void serialize(final ObjectData object) {
		object.setField("x", this.x);
		object.setField("y", this.y);
		object.setField("z", this.z);

		object.setField("name", this.name);

		object.setField("id", this.id);
	}

	@Override
	public String toString() {
		return "{\"x\":" + this.x + ",\"y\":" + this.y + ",\"z\":" + this.z + ",\"name\":\"" + this.name + "\",\"id\":"
				+ this.id + "}";
	}
}