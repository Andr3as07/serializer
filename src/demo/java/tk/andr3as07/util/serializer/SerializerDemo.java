package tk.andr3as07.util.serializer;

import java.io.FileOutputStream;

import tk.andr3as07.util.serializer.data.*;

public class SerializerDemo {

	public static void main(final String[] args) {
		final ObjectData db = new ObjectData();

		// Primitive Array
		final ArrayData array = new ArrayData(Serializer.TYPE_INTEGER, new int[] { 3, 3, 4 });

		array.set(2, PrimitiveData.fromInteger(17));
		array.set(new int[] { 0, 1, 1 }, PrimitiveData.fromInteger(25157));
		array.set(15, PrimitiveData.fromInteger(1024));

		db.setField("array", array);

		// String Array
		final ArrayData stringArray = new ArrayData(Serializer.TYPE_STRING, new int[] { 3, 4 });

		stringArray.set(2, PrimitiveData.fromString("Hey There!"));

		db.setField("strarray", stringArray);

		// Entity
		final Entity entity = new Entity(2.3f, 1.3f, 2.5f, "Andr3as07", 25157);

		db.setField("entity", entity);

		// List
		ListData list = new ListData(Serializer.TYPE_SHORT);

		list.add(PrimitiveData.fromShort((short) 17));
		list.add(PrimitiveData.fromShort((short) 25157));
		list.add(PrimitiveData.fromShort((short) 1024));

		db.setField("list", list);

		// Map
		MapData map = new MapData(Serializer.TYPE_LONG, Serializer.TYPE_STRING);

		map.put(PrimitiveData.fromLong(17), PrimitiveData.fromString("Andr3as07"));

		db.setField("map", map);

		final byte[] data = db.getSerializedData();
		write(data);

		final ObjectData obj = new ObjectData(data);

		System.out.println(((MapData) obj.getField("map")).getData());

		for (int i = 0; i < ((ArrayData) obj.getField("strarray")).getData().length; i++)
			System.out.println(i + ": " + ((ArrayData) obj.getField("strarray")).getData()[i]);
		

		System.out.println(((ListData) db.getField("list")).getData());
		System.out.println(db.getField("entity"));
		
		final ArrayData a = (ArrayData) obj.getField("array");

		for (int x = 0; x < 3; x++)
			for (int y = 0; y < 3; y++)
				for (int z = 0; z < 4; z++)
					System.out.println(x + ":" + y + ":" + z + ": " + a.get(new int[] { x, y, z }));
	}

	private static void write(final byte[] src) {
		try {
			final FileOutputStream stream = new FileOutputStream("stream.bin");
			stream.write(src);
			stream.close();
		} catch (final Exception ex) {
			// ToDo: handle exception
		}
	}
}