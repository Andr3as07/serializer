package tk.andr3as07.util.serializer;

import java.nio.charset.StandardCharsets;

public class Serializer {

	public static final byte TYPE_UNKNOWN = 0x00;

	public static final byte TYPE_BYTE = 0x01;
	public static final byte TYPE_SHORT = 0x02;
	public static final byte TYPE_INTEGER = 0x03;
	public static final byte TYPE_LONG = 0x04;
	public static final byte TYPE_FLOAT = 0x05;
	public static final byte TYPE_DOUBLE = 0x06;
	public static final byte TYPE_CHAR = 0x07;
	public static final byte TYPE_BOOLEAN = 0x08;
	public static final byte TYPE_STRING = 0x09;

	public static final byte TYPE_OBJECT = 0x10;
	public static final byte TYPE_LIST = 0x20;
	public static final byte TYPE_MAP = 0x30;
	public static final byte TYPE_ARRAY = 0x40;

	public static final byte WILDCARD_JUMP = (byte) 0x00FE;
	public static final byte WILDCARD_NULL = (byte) 0x00FF;

	public static boolean isPrimitive(final byte type) {
		switch (type) {
		case TYPE_BYTE:
		case TYPE_SHORT:
		case TYPE_INTEGER:
		case TYPE_LONG:
		case TYPE_FLOAT:
		case TYPE_DOUBLE:
		case TYPE_CHAR:
		case TYPE_BOOLEAN:
		case TYPE_STRING:
			return true;

		default:
			return false;
		}
	}

	public static boolean readBoolean(final byte[] src, final int pointer) {
		return src[pointer] != 0;
	}

	public static byte readByte(final byte[] src, final int pointer) {
		return src[pointer];
	}

	public static byte[] readBytes(final byte[] src, int pointer, final int length) {
		final byte[] array = new byte[length];
		for (int i = 0; i < length; i++)
			array[i] = readByte(src, pointer++);
		return array;
	}

	public static char readChar(final byte[] src, final int pointer) {
		return (char) (src[pointer] << 8 | src[pointer + 1]);
	}

	public static double readDouble(final byte[] src, final int pointer) {
		return Double.longBitsToDouble(readLong(src, pointer));
	}

	public static float readFloat(final byte[] src, final int pointer) {
		return Float.intBitsToFloat(readInteger(src, pointer));
	}

	public static int readInteger(final byte[] src, final int pointer) {
		return src[pointer] << 24 | src[pointer + 1] << 16 | src[pointer + 2] << 8 | src[pointer + 3];
	}

	public static long readLong(final byte[] src, final int pointer) {
		return src[pointer] << 56 | src[pointer + 1] << 48 | src[pointer + 2] << 40 | src[pointer + 3] << 32
				| src[pointer + 4] << 24 | src[pointer + 5] << 16 | src[pointer + 6] << 8 | src[pointer + 7];
	}

	public static short readShort(final byte[] src, final int pointer) {
		return (short) (src[pointer] << 8 | src[pointer + 1]);
	}

	public static String readString(final byte[] src, int pointer) {
		final int length = readInteger(src, pointer);
		final byte[] array = readBytes(src, pointer += 4, length);
		return new String(array, StandardCharsets.UTF_8);
	}

	public static int writeBytes(final byte[] destination, int pointer, final byte value) {
		destination[pointer++] = value;
		return pointer;
	}

	public static int writeBytes(final byte[] destination, int pointer, final byte[] values) {
		for (final byte element : values)
			destination[pointer++] = element;
		return pointer;
	}

	public static int writeBytes(final byte[] destination, int pointer, final byte[] values, final int length) {
		for (int i = 0; i < length; i++)
			destination[pointer++] = values[i];
		return pointer;
	}

	public static int writeBytes(final byte[] destination, int pointer, final char value) {
		destination[pointer++] = (byte) (value >> 8 & 0xFF);
		destination[pointer++] = (byte) (value >> 0 & 0xFF);
		return pointer;
	}

	public static int writeBytes(final byte[] destination, final int pointer, final double value) {
		return writeBytes(destination, pointer, Double.doubleToLongBits(value));
	}

	public static int writeBytes(final byte[] destination, final int pointer, final float value) {
		return writeBytes(destination, pointer, Float.floatToIntBits(value));
	}

	public static int writeBytes(final byte[] destination, int pointer, final int value) {
		destination[pointer++] = (byte) (value >> 24 & 0xFF);
		destination[pointer++] = (byte) (value >> 16 & 0xFF);
		destination[pointer++] = (byte) (value >> 8 & 0xFF);
		destination[pointer++] = (byte) (value >> 0 & 0xFF);
		return pointer;
	}

	public static int writeBytes(final byte[] destination, int pointer, final long value) {
		destination[pointer++] = (byte) (value >> 56 & 0xFF);
		destination[pointer++] = (byte) (value >> 48 & 0xFF);
		destination[pointer++] = (byte) (value >> 40 & 0xFF);
		destination[pointer++] = (byte) (value >> 32 & 0xFF);
		destination[pointer++] = (byte) (value >> 24 & 0xFF);
		destination[pointer++] = (byte) (value >> 16 & 0xFF);
		destination[pointer++] = (byte) (value >> 8 & 0xFF);
		destination[pointer++] = (byte) (value >> 0 & 0xFF);
		return pointer;
	}

	public static int writeBytes(final byte[] destination, int pointer, final short value) {
		destination[pointer++] = (byte) (value >> 8 & 0xFF);
		destination[pointer++] = (byte) (value >> 0 & 0xFF);
		return pointer;
	}

	public static int writeBytes(final byte[] destination, int pointer, final String value) {
		final byte[] data = value.getBytes(StandardCharsets.UTF_8);
		pointer = writeBytes(destination, pointer, data.length);
		pointer = writeBytes(destination, pointer, data);
		return pointer;
	}
}