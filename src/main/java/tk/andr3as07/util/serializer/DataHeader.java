package tk.andr3as07.util.serializer;

import static tk.andr3as07.util.serializer.Serializer.writeBytes;

import java.util.Arrays;

public class DataHeader {

	public byte size;
	public byte[] identifier;
	public byte type;

	public DataHeader(final byte[] identifier, final byte type) {
		this.size = (byte) identifier.length;
		this.identifier = identifier;
		this.type = type;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final DataHeader other = (DataHeader) obj;
		if (!Arrays.equals(this.identifier, other.identifier))
			return false;
		if (this.size != other.size)
			return false;
		return true;
	}

	public byte[] getData() {
		final byte[] result = new byte[this.getSize()];

		int pointer = writeBytes(result, 0, this.size);
		pointer = writeBytes(result, pointer, this.identifier);
		writeBytes(result, pointer, this.type);

		return result;
	}

	public int getSize() {
		return 1 + this.size + 1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.identifier);
		result = prime * result + this.size;
		return result;
	}

	@Override
	public String toString() {
		return "{\"name\":\"" + new String(this.identifier) + "\",\"type\":" + this.type + "}";
	}
}