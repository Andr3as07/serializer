package tk.andr3as07.util.serializer;

import tk.andr3as07.util.serializer.data.ObjectData;

public interface ISerializable {

	public void deserialize(ObjectData object);

	public void serialize(ObjectData object);

}