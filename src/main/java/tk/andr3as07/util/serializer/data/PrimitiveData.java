package tk.andr3as07.util.serializer.data;

import static tk.andr3as07.util.serializer.Serializer.*;

import java.util.Arrays;

import tk.andr3as07.util.serializer.Serializer;

public class PrimitiveData implements IData {

	public static final PrimitiveData NULL = new PrimitiveData(null, TYPE_UNKNOWN);

	public static PrimitiveData fromBoolean(final boolean value) {
		final PrimitiveData self = new PrimitiveData(new byte[] { (byte) (value ? 0x01 : 0x00) }, TYPE_BOOLEAN);
		return self;
	}

	public static PrimitiveData fromByte(final byte value) {
		final PrimitiveData self = new PrimitiveData(new byte[] { value }, TYPE_BYTE);
		return self;
	}

	public static PrimitiveData fromChar(final char value) {
		final byte[] data = new byte[4];
		Serializer.writeBytes(data, 0, value);
		final PrimitiveData self = new PrimitiveData(data, TYPE_CHAR);
		return self;
	}

	public static PrimitiveData fromData(final byte[] source, final byte type, final int pointer) {
		return fromData(Arrays.copyOfRange(source, pointer, source.length), type);
	}

	public static PrimitiveData fromData(final byte[] data, final byte type) {
		if (!Serializer.isPrimitive(type))
			return NULL;

		switch (type) {
		case TYPE_BOOLEAN:
			return fromBoolean(Serializer.readBoolean(data, 0));
		case TYPE_BYTE:
			return fromByte(data[0]);
		case TYPE_SHORT:
			return fromShort(Serializer.readShort(data, 0));
		case TYPE_INTEGER:
			return fromInteger(Serializer.readInteger(data, 0));
		case TYPE_LONG:
			return fromLong(Serializer.readLong(data, 0));
		case TYPE_CHAR:
			return fromChar(Serializer.readChar(data, 0));
		case TYPE_FLOAT:
			return fromFloat(Serializer.readFloat(data, 0));
		case TYPE_DOUBLE:
			return fromDouble(Serializer.readDouble(data, 0));
		case TYPE_STRING:
			return fromString(Serializer.readString(data, 0));
		}
		return NULL;
	}

	public static PrimitiveData fromDouble(final double value) {
		final byte[] data = new byte[8];
		Serializer.writeBytes(data, 0, value);
		final PrimitiveData self = new PrimitiveData(data, TYPE_DOUBLE);
		return self;
	}

	public static PrimitiveData fromFloat(final float value) {
		final byte[] data = new byte[4];
		Serializer.writeBytes(data, 0, value);
		final PrimitiveData self = new PrimitiveData(data, TYPE_FLOAT);
		return self;
	}

	public static PrimitiveData fromInteger(final int value) {
		final byte[] data = new byte[4];
		Serializer.writeBytes(data, 0, value);
		final PrimitiveData self = new PrimitiveData(data, TYPE_INTEGER);
		return self;
	}

	public static PrimitiveData fromLong(final long value) {
		final byte[] data = new byte[8];
		Serializer.writeBytes(data, 0, value);
		final PrimitiveData self = new PrimitiveData(data, TYPE_LONG);
		return self;
	}

	public static PrimitiveData fromShort(final short value) {
		final byte[] data = new byte[2];
		Serializer.writeBytes(data, 0, value);
		final PrimitiveData self = new PrimitiveData(data, TYPE_SHORT);
		return self;
	}

	public static PrimitiveData fromString(final String value) {
		final byte[] data = new byte[value.length() + 4];
		Serializer.writeBytes(data, 0, value);
		final PrimitiveData self = new PrimitiveData(data, TYPE_STRING);
		return self;
	}

	protected byte[] value;

	protected byte type;

	PrimitiveData(final byte[] value, final byte type) {
		this.value = value;
		this.type = type;
	}

	public boolean getBoolean() {
		if (this.value == null || this.value.length != 1)
			return false;
		return Serializer.readBoolean(this.value, 0);
	}

	public byte getByte() {
		if (this.value == null || this.value.length != 1)
			return (byte) 0;
		return Serializer.readByte(this.value, 0);
	}

	public char getChar() {
		if (this.value == null || this.value.length != 4)
			return '\u0000';
		return Serializer.readChar(this.value, 0);
	}

	@Override
	public Object getData() {
		switch (this.type) {
		case TYPE_BOOLEAN:
			return this.getBoolean();
		case TYPE_BYTE:
			return this.getByte();
		case TYPE_SHORT:
			return this.getShort();
		case TYPE_INTEGER:
			return this.getInteger();
		case TYPE_LONG:
			return this.getLong();
		case TYPE_CHAR:
			return this.getChar();
		case TYPE_FLOAT:
			return this.getFloat();
		case TYPE_DOUBLE:
			return this.getDouble();
		case TYPE_STRING:
			return this.getString();
		}
		return null;
	}

	public double getDouble() {
		if (this.value == null || this.value.length != 4)
			return 0.0d;
		return Serializer.readDouble(this.value, 0);
	}

	public float getFloat() {
		if (this.value == null || this.value.length != 4)
			return 0.0f;
		return Serializer.readFloat(this.value, 0);
	}

	public int getInteger() {
		if (this.value == null || this.value.length != 4)
			return 0;
		return Serializer.readInteger(this.value, 0);
	}

	public long getLong() {
		if (this.value == null || this.value.length != 8)
			return 0;
		return Serializer.readLong(this.value, 0);
	}

	@Override
	public byte[] getSerializedData() {
		return this.value;
	}

	@Override
	public int getSerializedSize() {
		return this.value.length;
	}

	public short getShort() {
		if (this.value == null || this.value.length != 2)
			return (short) 0;
		return Serializer.readShort(this.value, 0);
	}

	public String getString() {
		if (this.value == null || this.value.length < 4)
			return null;
		return Serializer.readString(this.value, 0);
	}

	public byte getType() {
		return this.type;
	}

	@Override
	public String toString() {
		return this.getData().toString();
	}
}