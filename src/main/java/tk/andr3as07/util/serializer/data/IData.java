package tk.andr3as07.util.serializer.data;

public interface IData {

	public Object getData();

	public byte[] getSerializedData();

	public int getSerializedSize();

}