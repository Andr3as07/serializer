package tk.andr3as07.util.serializer.data;

import static tk.andr3as07.util.serializer.Serializer.*;

import java.util.Arrays;

import tk.andr3as07.util.serializer.Serializer;

public class ArrayData implements IData {

	protected final byte type;
	protected final IData[] data;
	protected final int[] sizes;

	// public ArrayData(Object[][] array) {
	// int l = array.length;
	// int sizes[] = new int[l];
	// for(int i = 0; i < l; i++)
	// sizes[i] = array[i].length;
	//
	// // TODO: Figure out a way to detect the type
	//
	// this.sizes = sizes;
	// int size = sizes[0];
	// for(int i = 1; i < sizes.length; i++)
	// size *= sizes[i];
	// this.data = new IData[size];
	// }

	public ArrayData(final byte[] source, final int pointer) {
		this(Arrays.copyOfRange(source, pointer, source.length));
	}

	public ArrayData(final byte[] source) {
		this.type = Serializer.readByte(source, 0);
		int pointer = 1;

		final byte dimensions = Serializer.readByte(source, pointer);
		pointer++;

		final int[] sizes = new int[dimensions];
		for (int i = 0; i < dimensions; i++) {
			sizes[i] = Serializer.readInteger(source, pointer);
			pointer += 4;
		}

		this.sizes = sizes;

		int size = sizes[0];
		for (int i = 1; i < sizes.length; i++)
			size *= sizes[i];

		this.data = new IData[size];

		IData data = null;

		for (int i = 0; i < getTotalSize(); i++)
			if (Serializer.readByte(source, pointer) == WILDCARD_NULL)
				pointer++;
			else if (Serializer.readByte(source, pointer) == WILDCARD_JUMP) {
				final byte jump = (byte) (Serializer.readByte(source, pointer + 1) - 1);
				pointer += 2;
				i += jump;
			} else {
				if (Serializer.isPrimitive(type))
					data = PrimitiveData.fromData(source, type, pointer);
				else if (type == TYPE_OBJECT)
					data = new ObjectData(source, pointer);
				else if (type == Serializer.TYPE_ARRAY)
					data = new ArrayData(source, pointer);
				else if (type == Serializer.TYPE_LIST)
					data = new ListData(source, pointer);
				else if (type == Serializer.TYPE_MAP)
					data = new MapData(source, pointer);
				pointer += data.getSerializedSize();
				set(i, data);
			}
	}

	public ArrayData(final byte type, final int size) {
		this.type = type;
		this.sizes = new int[] { size };
		this.data = new IData[size];
	}

	public ArrayData(final byte type, final int[] sizes) {
		this.type = type;
		this.sizes = sizes;

		int size = sizes[0];
		for (int i = 1; i < sizes.length; i++)
			size *= sizes[i];
		this.data = new IData[size];
	}

	public int dimensions() {
		return this.sizes.length;
	}

	public IData get(final int index) {
		return this.data[index];
	}

	public IData get(final int[] location) {
		return this.get(this.resolve(location));
	}

	@Override
	public IData[] getData() {
		return this.data;
	}

	@Override
	public byte[] getSerializedData() {
		final byte[] result = new byte[this.getSerializedSize()];
		int pointer = writeBytes(result, 0, this.type);
		pointer = writeBytes(result, pointer, (byte) this.dimensions());
		for (int d = 0; d < this.dimensions(); d++)
			pointer = writeBytes(result, pointer, this.size(d));

		byte consecutiveNull = 0;
		for (final IData item : this.data)
			if (item == null)
				consecutiveNull++; // pointer = writeBytes(result, pointer,
									// Serializer.WILDCARD_NULL);
			else {
				if (consecutiveNull < 3)
					for (byte i = 0; i < consecutiveNull; i++)
						pointer = writeBytes(result, pointer, Serializer.WILDCARD_NULL);
				else if (consecutiveNull != 0) {
					pointer = writeBytes(result, pointer, Serializer.WILDCARD_JUMP);
					pointer = writeBytes(result, pointer, consecutiveNull);
				}
				consecutiveNull = 0;
				pointer = writeBytes(result, pointer, item.getSerializedData());
			}
		if (consecutiveNull < 3)
			for (int i = 0; i < consecutiveNull; i++)
				pointer = writeBytes(result, pointer, Serializer.WILDCARD_NULL);
		else if (consecutiveNull != 0) {
			pointer = writeBytes(result, pointer, Serializer.WILDCARD_JUMP);
			pointer = writeBytes(result, pointer, consecutiveNull);
		}
		return result;
	}

	@Override
	public int getSerializedSize() {
		int size = 2 + this.dimensions() * 4;

		int consecutiveNull = 0;
		for (final IData item : this.data)
			if (item == null)
				consecutiveNull++;
			else {
				size += Math.min(consecutiveNull, 2);
				consecutiveNull = 0;
				size += item.getSerializedSize();
			}
		size += Math.min(consecutiveNull, 2);
		return size;
	}

	public int getTotalSize() {
		return this.data.length;
	}

	protected int[] resolve(final int index) {
		if (index < 0 || index >= this.getTotalSize())
			throw new IndexOutOfBoundsException();

		final int[] sizes = new int[this.dimensions()];

		int mod = index % this.size(0);
		int quotient = (index - mod) / this.size(0);
		sizes[0] = mod;

		for (int i = 1; i < this.dimensions(); i++) {
			mod = quotient % this.size(i);
			quotient = (quotient - mod) / this.size(i);

			sizes[i] = mod;
		}

		return sizes;
	}

	protected int resolve(final int[] sizes) {
		if (sizes.length != this.dimensions())
			throw new IndexOutOfBoundsException();

		int multiplier = 1;
		int result = 0;
		for (int i = 0; i < this.dimensions(); i++) {
			result += sizes[i] * multiplier;
			multiplier *= this.size(i);
		}
		return result;
	}

	public void set(final int index, final IData value) {
		this.data[index] = value;
	}

	public void set(final int[] location, final IData value) {
		this.set(this.resolve(location), value);
	}

	public int size(final int dimension) {
		return this.sizes[dimension];
	}

}