package tk.andr3as07.util.serializer.data;

import static tk.andr3as07.util.serializer.Serializer.*;

import java.util.*;

import tk.andr3as07.util.serializer.Serializer;

public class MapData implements IData {

	protected byte key, value;
	protected Map<IData, IData> fields;


	public MapData(final byte[] source) {
		int pointer = 0;

		this.key = Serializer.readByte(source, pointer);
		pointer++;
		
		this.value = Serializer.readByte(source, pointer);
		pointer++;

		int size = Serializer.readInteger(source, pointer);
		pointer += 4;

		this.fields = new HashMap<>();
		
		for (int i = 0; i < size; i++) {
			IData keyData = null;
			IData valueData = null;

			if (Serializer.isPrimitive(key))
				keyData = PrimitiveData.fromData(source, key, pointer);
			else if (key == TYPE_OBJECT)
				keyData = new ObjectData(source, pointer);
			else if (key == TYPE_ARRAY)
				keyData = new ArrayData(source, pointer);
			else if (key == TYPE_LIST)
				keyData = new ListData(source, pointer);
			else if (key == TYPE_MAP)
				keyData = new MapData(source, pointer);
			pointer += keyData.getSerializedSize();

			if (Serializer.isPrimitive(value))
				valueData = PrimitiveData.fromData(source, value, pointer);
			else if (value == TYPE_OBJECT)
				valueData = new ObjectData(source, pointer);
			else if (value == TYPE_ARRAY)
				valueData = new ArrayData(source, pointer);
			else if (value == TYPE_LIST)
				valueData = new ListData(source, pointer);
			else if (value == TYPE_MAP)
				valueData = new MapData(source, pointer);
			pointer += valueData.getSerializedSize();

			fields.put(keyData, valueData);
		}
	}

	public MapData(final byte[] source, final int pointer) {
		this(Arrays.copyOfRange(source, pointer, source.length));
	}
	
	public MapData(final byte key, final byte value) {
		this.fields = new HashMap<>();
		this.key = key;
		this.value = value;
	}

	@Override
	public Object getData() {
		return this.fields;
	}

	@Override
	public byte[] getSerializedData() {
		final byte[] result = new byte[this.getSerializedSize()];
		int pointer = Serializer.writeBytes(result, 0, this.key);
		pointer = Serializer.writeBytes(result, pointer, this.value);
		pointer = Serializer.writeBytes(result, pointer, this.fields.size());
		for (final IData key : this.fields.keySet()) {
			pointer = writeBytes(result, pointer, key.getSerializedData());
			pointer = writeBytes(result, pointer, this.fields.get(key).getSerializedData());
		}
		return result;
	}

	@Override
	public int getSerializedSize() {
		int size = 6;
		for (final IData key : this.fields.keySet()) {
			size += key.getSerializedSize();
			size += this.fields.get(key).getSerializedSize();
		}
		return size;
	}

	public IData put(final IData key, final IData value) {
		return this.fields.put(key, value);
	}
	
	public IData get(IData key) {
		return this.fields.get(key);
	}

}