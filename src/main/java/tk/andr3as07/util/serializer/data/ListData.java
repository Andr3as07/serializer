package tk.andr3as07.util.serializer.data;

import static tk.andr3as07.util.serializer.Serializer.*;

import java.util.*;

import tk.andr3as07.util.serializer.Serializer;

public class ListData implements IData {

	protected byte type;
	protected List<IData> items;

	public ListData(final byte[] source) {
		int pointer = 0;
		this.type = Serializer.readByte(source, pointer);
		pointer++;

		ListData list = new ListData(type);

		int size = Serializer.readInteger(source, pointer);
		pointer += 4;

		this.items = new ArrayList<>();

		for (int i = 0; i < size; i++) {
			IData data = null;

			if (Serializer.isPrimitive(type))
				data = PrimitiveData.fromData(source, type, pointer);
			else if (type == TYPE_OBJECT)
				data = new ObjectData(source, pointer);
			else if (type == TYPE_ARRAY)
				data = new ArrayData(source, pointer);
			else if (type == TYPE_LIST)
				data = new ListData(source, pointer);
			else if (type == TYPE_MAP)
				data = new MapData(source, pointer);
			pointer += data.getSerializedSize();
			items.add(data);
		}
	}

	public ListData(final byte[] source, final int pointer) {
		this(Arrays.copyOfRange(source, pointer, source.length));
	}
	
	public ListData(final byte type) {
		this.items = new ArrayList<>();
		this.type = type;
	}

	public boolean add(final IData data) {
		return this.items.add(data);
	}

	public void add(final int index, final IData data) {
		this.items.add(index, data);
	}

	@Override
	public Object getData() {
		return Collections.unmodifiableList(items);
	}

	@Override
	public byte[] getSerializedData() {
		final byte[] result = new byte[this.getSerializedSize()];
		int pointer = writeBytes(result, 0, this.type);
		pointer = writeBytes(result, pointer, this.items.size());
		for (final IData item : this.items)
			pointer = writeBytes(result, pointer, item.getSerializedData());
		return result;
	}

	@Override
	public int getSerializedSize() {
		int size = 5;
		for (final IData item : this.items)
			size += item.getSerializedSize();
		return size;
	}

	public boolean remove(final IData data) {
		return this.items.remove(data);
	}

	public IData remove(final int index) {
		return this.items.remove(index);
	}
}