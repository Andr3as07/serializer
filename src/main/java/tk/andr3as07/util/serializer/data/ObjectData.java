package tk.andr3as07.util.serializer.data;

import static tk.andr3as07.util.serializer.Serializer.*;

import java.nio.charset.StandardCharsets;
import java.util.*;

import tk.andr3as07.util.serializer.*;

public class ObjectData implements IData {

	public ObjectData(final byte[] source) {
		this();
		int pointer = 0;
		final short size = Serializer.readShort(source, pointer);
		pointer += 2;

		// Classname
		if (Serializer.readByte(source, pointer) == WILDCARD_NULL) {
			pointer++;
			name = null;
		} else {
			final String className = readClassIdentifier(source, pointer);
			pointer += 2 + className.length();
			name = className;
		}

		for (short s = 0; s < size; s++) {
			// Name
			final byte length = Serializer.readByte(source, pointer);
			pointer++;
			final byte[] name = Serializer.readBytes(source, pointer, length);
			pointer += length;

			// Type
			final byte type = Serializer.readByte(source, pointer);
			pointer++;

			final DataHeader header = new DataHeader(name, type);

			IData data = null;

			// Data
			if (Serializer.isPrimitive(type))
				data = PrimitiveData.fromData(source, type, pointer);
			else if (type == TYPE_OBJECT)
				data = new ObjectData(source, pointer);
			else if (type == TYPE_ARRAY)
				data = new ArrayData(source, pointer);
			else if (type == TYPE_LIST)
				data = new ListData(source, pointer);
			else if (type == TYPE_MAP)
				data = new MapData(source, pointer);
			pointer += data.getSerializedSize();
			setField(header, data);
		}
	}

	public ObjectData(final byte[] source, final int pointer) {
		this(Arrays.copyOfRange(source, pointer, source.length));
	}

	private static String readClassIdentifier(final byte[] src, int pointer) {
		final short length = readShort(src, pointer);
		final byte[] array = readBytes(src, pointer += 2, length);
		return new String(array, StandardCharsets.UTF_8);
	}
	
	private static int writeClassIdentifier(final byte[] destination, int pointer, final String value) {
		final byte[] data = value.getBytes(StandardCharsets.UTF_8);
		assert data.length <= Short.MAX_VALUE;
		pointer = writeBytes(destination, pointer, (short) data.length);
		pointer = writeBytes(destination, pointer, data);
		return pointer;
	}

	private String name;

	protected Map<DataHeader, IData> fields;

	public ObjectData() {
		this.fields = new HashMap<>();
	}

	ObjectData(final Class<?> cls) {
		this(cls.getName());
	}

	ObjectData(final String name) {
		this();
		this.name = name;
	}

	@Override
	public Object getData() {
		ISerializable obj;
		try {
			obj = (ISerializable) Class.forName(this.name).newInstance();
			obj.deserialize(this);
			return obj;
		} catch (final Exception e) {
			// TODO: FIX
			e.printStackTrace();
		}
		return null;
	}

	public IData getField(final String name) {
		final DataHeader dataHeader = new DataHeader(name.getBytes(), TYPE_UNKNOWN);
		for (final DataHeader header : this.fields.keySet())
			if (header.equals(dataHeader))
				return this.fields.get(header);
		return null;
	}

	public String getName() {
		return this.name;
	}

	private int getNameLength() {
		if (this.isNamed())
			return this.name.length() + 2; // Data + Length
		else
			return 1; // WILDCARD_NULL
	}

	public PrimitiveData getPrimitiveField(final String name) {
		final IData data = this.getField(name);

		if (data == null)
			return PrimitiveData.NULL;

		if (data instanceof PrimitiveData)
			return (PrimitiveData) data;

		return PrimitiveData.NULL;
	}

	@Override
	public byte[] getSerializedData() {
		final byte[] result = new byte[this.getSerializedSize()];
		int pointer = Serializer.writeBytes(result, 0, (short) this.fields.size());
		if (this.isNamed())
			pointer = writeClassIdentifier(result, pointer, this.name);
		else
			pointer = Serializer.writeBytes(result, pointer, Serializer.WILDCARD_NULL);
		for (final DataHeader header : this.fields.keySet()) {
			pointer = writeBytes(result, pointer, header.getData());
			pointer = writeBytes(result, pointer, this.fields.get(header).getSerializedData());
		}
		return result;
	}

	@Override
	public int getSerializedSize() {
		int size = 2; // fields (short)
		size += this.getNameLength();
		for (final DataHeader header : this.fields.keySet()) {
			size += header.getData().length;
			size += this.fields.get(header).getSerializedSize();
		}
		return size;
	}

	public boolean isNamed() {
		return this.name != null;
	}

	IData setField(final DataHeader header, final IData data) {
		return this.fields.put(header, data);
	}

	public IData setField(final String name, final ArrayData value) {
		final DataHeader header = new DataHeader(name.getBytes(), TYPE_ARRAY);
		return this.fields.put(header, value);
	}

	public IData setField(final String name, final boolean value) {
		return this.setField(name, PrimitiveData.fromBoolean(value));
	}

	public IData setField(final String name, final byte value) {
		return this.setField(name, PrimitiveData.fromByte(value));
	}

	public IData setField(final String name, final char value) {
		return this.setField(name, PrimitiveData.fromChar(value));
	}

	public IData setField(final String name, final double value) {
		return this.setField(name, PrimitiveData.fromDouble(value));
	}

	public IData setField(final String name, final float value) {
		return this.setField(name, PrimitiveData.fromFloat(value));
	}

	public IData setField(final String name, final int value) {
		return this.setField(name, PrimitiveData.fromInteger(value));
	}

	public IData setField(final String name, final ISerializable value) {
		final DataHeader header = new DataHeader(name.getBytes(), TYPE_OBJECT);
		final ObjectData data = new ObjectData(value.getClass());
		value.serialize(data);
		return this.fields.put(header, data);
	}

	public IData setField(final String name, final ListData value) {
		final DataHeader header = new DataHeader(name.getBytes(), TYPE_LIST);
		return this.fields.put(header, value);
	}

	public IData setField(final String name, final long value) {
		return this.setField(name, PrimitiveData.fromLong(value));
	}

	public IData setField(final String name, final MapData value) {
		final DataHeader header = new DataHeader(name.getBytes(), TYPE_MAP);
		return this.fields.put(header, value);
	}

	public IData setField(final String name, final ObjectData value) {
		final DataHeader header = new DataHeader(name.getBytes(), TYPE_OBJECT);
		return this.fields.put(header, value);
	}

	public IData setField(final String name, final PrimitiveData value) {
		return this.setField(name, value, value.getType());
	}

	private IData setField(final String name, final PrimitiveData data, final byte type) {
		final DataHeader header = new DataHeader(name.getBytes(), type);
		return this.fields.put(header, data);
	}

	public IData setField(final String name, final short value) {
		return this.setField(name, PrimitiveData.fromShort(value));
	}

	public IData setField(final String name, final String value) {
		return this.setField(name, PrimitiveData.fromString(value));
	}

	@Override
	public String toString() {
		return "{\"class\":\"" + this.name + "\",\"fields\":" + this.fields.toString() + "}";
	}
}