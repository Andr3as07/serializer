# Semantic Versioning 2.0.0

## Summary

Given a version number MAJOR.MINOR.PATCH, increment the:

* MAJOR version when you make incompatible API changes,
* MINOR version when you add functionality in a backwards-compatible manner, and
* PATCH version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the ** MAJOR.MINOR.PATCH ** format.

## About

The Semantic Versioning specification is authored by [Tom Preston-Werner](http://tom.preston-werner.com/ "tom.preston-werner.com"), inventor of Gravatars and cofounder of GitHub.
